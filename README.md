# qr2cairo

<!-- cargo-sync-readme start -->

Vector drawing of QR code using Cairo.

## Examples

### PDF

PDF example from the `examples/` directory:
```rust
let surface = cairo::PdfSurface::new(100.0, 100.0, "example.pdf").unwrap();
let cr = cairo::Context::new(&surface);
qr2cairo::draw(&cr, 100.0, 100.0, "qr2cairo").unwrap();
cr.show_page();
```

This generates this PDF: [example.pdf](https://www.willemp.be/qr2cairo/example.pdf).

### GTK 

GTK example (note: no GTK required to use qr2cairo):

```sh
$ cd examples/gtk ; cargo run
```

![qr2cairo gtk example](https://www.willemp.be/qr2cairo/screenshot-qr2cairo-0.1.0-gtk.svg)

### SVG

The underlying qrcode crate can
[create SVGs](https://docs.rs/qrcode/0.12.0/qrcode/render/svg/index.html).

<!-- cargo-sync-readme end -->
