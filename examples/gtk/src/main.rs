// To allow you to freely copy-paste from this example,
// this example is licensed under the "unlicense" license:
// - - - - - - - - - - - - -  - - - - - - - - - - - - -  - - - - - - - - -
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// - - - - - - - - - - - - -  - - - - - - - - - - - - -  - - - - - - - - -

///! Example GTK application showing QR code of user-entered text.
///!
///! ![qr2cairo gtk example](../res/screenshot-qr2cairo-gtk.svg)

extern crate gtk;
extern crate qr2cairo;
use crate::gtk::BoxExt;
use crate::gtk::ContainerExt;
use crate::gtk::EditableSignals;
use crate::gtk::EntryExt;
use crate::gtk::GtkWindowExt;
use crate::gtk::WidgetExt;
use gtk::Inhibit;
use std::convert::TryInto;

fn main() {
    gtk::init().unwrap();
    let window = gtk::Window::new(gtk::WindowType::Toplevel);
    window.set_default_size(200, 230);
    window.set_title("qr2cairo");
    let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
    let drawing_area = gtk::DrawingArea::new();
    let entry = gtk::Entry::new();
    entry.set_text("qr2cairo");
    vbox.pack_start(&drawing_area, true, true, 0);
    vbox.add(&entry);
    window.add(&vbox);
    
    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    // When the text changes, the QR code must be redrawn.
    let drawing_area_shallow_copy = drawing_area.clone();
    entry.connect_changed(move |_| {
        drawing_area_shallow_copy.queue_draw();
    });

    // The drawing callback of GTK calls qr2cairo to (re)draw the QR code.
    drawing_area.connect_draw(move |drawing_area, cr| {
        let width = drawing_area.get_allocated_width().try_into().unwrap();
        let height = drawing_area.get_allocated_height().try_into().unwrap();
        let text = entry.get_text().unwrap();
        let _ = qr2cairo::draw(cr, width, height, &text.as_str());
        Inhibit(false)
    });

    window.show_all();
    gtk::main();
}
